import express, { Express, Request, Response } from "express";
import router from "./project/router";

import { MongoClient, ServerApiVersion } from "mongodb";


import ololog from "ololog";

const log = ololog;


const MONGO_URI = "mongodb+srv://neonkuroasa:HqLBE34JT8agiYqK@cluster0.9ti4goh.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0";
const mongo = new MongoClient(MONGO_URI, {
  serverApi: {
    version: ServerApiVersion.v1,
    strict: true,
    deprecationErrors: true
  }
})


async function connect_to_mongodb() {
  try {
    await mongo.connect();
    await mongo.db("admin").command({ ping: 1 });

    // start server main loop here
    start_express_server(mongo);

  } finally {
    await mongo.close();
  }
}

/**
 * Starts the express server
 */
function start_express_server(mongo: MongoClient) {
  const app: Express = express();
  const PORT: number = 3000;

  app.set("mongo", mongo)

  app.use((req, _res, next) => {
    log(`${req.method} ${req.path}`)
    next();
  })

  app.get("/", function(_req: Request, res: Response) {
    res.send("Ciao mon pote! XPTDR");
  })

  app.use("/project", router);

  app.listen(PORT, () => {
    console.log("Le serveur il est lancé, wesh")
  })
}


connect_to_mongodb().catch(console.dir)
