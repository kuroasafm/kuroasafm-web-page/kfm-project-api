import { Router } from "express";
import { MongoClient } from "mongodb";

const router: Router = Router();

router.get("/", async (req, res) => {
  // res.send("Hello depuis le router")

  const mongo: MongoClient = req.app.get("mongo")
  await mongo.connect();

  var projects = await mongo.db("kuroasafm").collection("projects").findOne();

  res.json(projects)

})


export default router;

